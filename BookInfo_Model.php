<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class BookInfo_Model extends CI_Model{
	public function __construct(){
		$this->load->database();
		//create table & insert sample values
		/*$this->db->query("CREATE TABLE bookinfo (id int NOT NULL AUTO_INCREMENT, bookName varchar(255) NOT NULL, 
							authorName varchar(255) NOT NULL, amount int NOT NULL, salePrice int, PRIMARY KEY (id))");
		$this->db->query("INSERT INTO bookinfo (bookName,authorName,amount,salePrice) VALUES ('Alchemist','Paulo',299,199)");
		$this->db->query("INSERT INTO bookinfo (bookName,authorName,amount,salePrice) VALUES ('Narnia','Lewis',399,299)");
		$this->db->query("INSERT INTO bookinfo (bookName,authorName,amount,salePrice) VALUES ('Brida','Paulo',199,99)");*/
	}
	public function addbook($bookName, $authorName, $amount, $salePrice){
		$record = array(
			'bookName' => $bookName,
			'authorName' => $authorName,
			'amount' => $amount,
			'salePrice' => $salePrice
		);
		$this->db->insert('bookinfo', $record);
		/*$this->db->query("INSERT INTO bookinfo (bookName, authorName, amount, salePrice) VALUES ('$bookName', '$authorName', '$amount', '$salePrice')");*/
	}
	public function editbook($id, $bookName, $authorName, $amount, $salePrice){
		$record = array(
			'id' => $id,
			'bookName' => $bookName,
			'authorName' => $authorName,
			'amount' => $amount,
			'salePrice' => $salePrice
		);
		$this->db->replace('bookinfo', $record);
		/*$this->db->query("UPDATE  bookinfo SET id = '$id',bookName = '$bookName' ,authorName = '$authorName', amount = '$amount' , salePrice = '$salePrice' WHERE id = '$id'");*/
	}
	public function deletebook($id){
		//$this->db->delete('bookinfo',array('id' => $id));
		$this->db->query("DELETE FROM bookinfo WHERE id = '$id'");
	}
	public function readbooks(){
		//return $this->db->get('bookinfo');
		$result = $this->db->query("SELECT * FROM bookinfo");
		$result = $result->result_array();
		$data = [];
		foreach ($result as $key => $value) {
			/*echo "VALUE\n";
			var_dump($value);*/
			$itm = $price = [];
			/*echo "PRICE\n";
			var_dump($price);*/
			foreach ($value as $subkey => $subvalue) {
				/*echo "SUBKEY\n";
				var_dump($subkey);
				echo "SUBVALUE\n";
				var_dump($subvalue);*/
				if($subkey == 'amount' || $subkey == 'salePrice'){
					$price += [$subkey => $subvalue];
				}
				else $itm += [$subkey => $subvalue];
			}
			$itm += ['price' => $price];
			$data += [$key=>$itm];
			/*echo "PRICE\n";
			var_dump($price);
			echo "DATA\n";
			var_dump($data);*/
		}
		$json_data = ['Status' => 200, 'Data' => $data];
		return json_encode($json_data);
		//return var_dump($data);
	}
	public function readbook($authorName){
		//return $this->db->get_where('bookinfo',array('authorName' => $authorName));
		$result = $this->db->query("SELECT id, bookName, amount, salePrice FROM bookinfo WHERE authorName = '$authorName'");
		$result = $result->result_array();
		$data = [];
		foreach ($result as $key => $value) {
			$itm = $price = [];
			foreach ($value as $subkey => $subvalue) {
				if($subkey == 'amount' || $subkey == 'salePrice'){
					$price += [$subkey => $subvalue];
				}
				else $itm += [$subkey => $subvalue];
			}
			$itm += ['price' => $price];
			$data += [$key=>$itm];
		}
		$json_data= ['Status' => 200, 'Data' => ['Author name' => $authorName, 'Book Info' => $data]];
		return json_encode($json_data);
		//return var_dump($json_data);
	}
}
?>