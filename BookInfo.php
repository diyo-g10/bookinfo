<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class BookInfo extends CI_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->model('BookInfo_Model','BIModel');
	}
	public function index(){
		echo "To add book record call: {base_url}/addBook/bookName/authorName/amount/[salePrice]<hr>";
		echo "To edit book record call: {base_url}/editBook/id/bookname/authorName/amount/[salePrice]<hr>";
		echo "To delete book record call: {base_url}/deleteBook/id<hr>";
		echo "To view all book records call: {base_url}/readBooks<hr>";
		echo "To view book records by author call: {base_url}/readBook/authorName<hr>";
		echo "<b>NOTE</b><br>salePrice is optional";
		echo "<br>base_url is 'http://localhost/codeigniter/index.php/BookInfo'";
		echo "<br>To create table & insert sample data comment out lines 6 to 10 in BookInfo_Model.php under models folder.";
	}
	public function addBook($bookName, $authorName, $amount, $salePrice=null){
		$this->BIModel->addbook($bookName, $authorName, $amount, $salePrice);
	}
	public function editBook($id, $bookName, $authorName, $amount, $salePrice=null){
		$this->BIModel->editbook($id, $bookName, $authorName, $amount, $salePrice);
	}
	public function deleteBook($id){
		$this->BIModel->deletebook($id);
	}
	public function readBooks(){
		$data_json = $this->BIModel->readbooks();
		//$result = $result->result_array();
		//$res_json = json_encode($result);
		header('Content-type: application/json', true, 200);
		echo $data_json;
	}
	public function readBook($authorName){
		$data_json = $this->BIModel->readbook($authorName);
		//$result = $result->result_array();
		//$res_json = json_encode($result);
		header('Content-type: application/json', true, 200);
		echo $data_json;
	}
}
?>