# README #

A simple API that returns JSON data

### How do I get set up? ###

- To add book record call: {base_url}/addBook/bookName/authorName/amount/[salePrice]   
- To edit book record call: {base_url}/editBook/id/bookname/authorName/amount/[salePrice]   
- To delete book record call: {base_url}/deleteBook/id   
- To view all book records call: {base_url}/readBooks   
- To view book records by author call: {base_url}/readBook/authorName   

### NOTE ####
- Place BookInfo.php under controllers folder in CodeIgniter   
- Place BookInfo_Model.php under models folder in CodeIgniter   
- base_url is 'http://localhost/codeigniter/index.php/BookInfo'   
- salePrice is optional   
- To create table & insert sample data comment out lines 6 to 10 in BookInfo_Model.php under models folder.   
